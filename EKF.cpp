#include <iostream>
#include <inttypes.h>
#include <armadillo>
#include <cmath>



using namespace std;
using namespace arma;

#define pi 3.14159265359

//void EKF(mat r_es, mat u_es, mat q_es, mat p1_es, mat p2_es, mat p3_es, mat p4_es, 

// Declare the used functions.
double factorial(uint16_t n);
mat Skew(mat vIn);
mat GAMMA(mat omega, double dt, double k);
mat quaternion2Matrix(mat quat);
mat measur(mat qk, mat pk1, mat pk2, mat pk3, mat pk4, mat rk);
mat rot_vector2quaternion(mat vIn);
void Disc_Dynamics(mat f, mat omega, mat q, double wf, double ww, double wbw, double wp1, double wp2, double wp3, double wp4, double dt, mat& Fd, mat& Qd);
mat quaternion_mult(mat qin, mat pin);



// Main Function
int main(int argc, char **argv)
{
  

int i = 100;

bool touch1 = true; 		// Boolean that checks the ground contact of the first foot.
bool touch2 = true; 		// Boolean that checks the ground contact of the second foot.
bool touch3 = true; 		// Boolean that checks the ground contact of the third foot.
bool touch4 = true; 		// Boolean that checks the ground contact of the fourth foot.
bool first_touch1 = true; 	// Boolean that checks if the event of ground contact of the first foot is the first.
bool first_touch2 = true;	// Boolean that checks if the event of ground contact of the second foot is the first.
bool first_touch3 = true;	// Boolean that checks if the event of ground contact of the third foot is the first.
bool first_touch4 = true;	// Boolean that checks if the event of ground contact of the fourth foot is the first.

bool first_call = true;		// Boolean used for initialization of the filter.

mat z_leg1(3,1), z_leg2(3,1), z_leg3(3,1), z_leg4(3,1); // These vectors correspond to the leg kinematics of each foot, i.e. the position of each 								// footpoint with respect to the COM of the robot. These are input to the EKF and 								// represent the measurement model.


//////////////////////////////////////////  Extended Kalman Filter   ///////////////////////////////////////////////////////

/* 
Sigma variables are related to the white noises which can be derived from the IMU datasheet. The sigmawpi, where i = {1,2,3,4}, can be set to a small value to handle slippage of the i-th foot.
*/

double sigmaf = 0.01;		// Variance of noise of the accelerometer. Check datasheet of the IMU.
double sigmaw = 0.044*pi/180;	// Variance of noise of the gyroscope.	
double sigmabw = 0.007*pi/180;	// Variance of noise of the bias of the gyroscope.
double sigmawp1 = 0.004;	// Variance of the noise of the footpoints. They may change to handle slippage.
double sigmawp2 = 0.004;	// 				>>>>
double sigmawp3 = 0.004;	//				>>>>
double sigmawp4 = 0.004;	//				>>>>

double wf, ww, wbw, wp1, wp2, wp3, wp4; 	// Noise densities of the above variances.
double wp1p, wp2p, wp3p, wp4p;			// Noise densities, used when a foot has ground contact.

double dt = 0.001; 		// This value can be set to the difference between two subsequent timer readings.



///////////////////////////////////////// STATE VARIABLES //////////////////////////

mat r_es(3,1);			// Position of the COM of the robot in the inertial frame.
mat u_es(3,1);			// Velocity of the COM of the robot in the inertial frame.
mat q_es(4,1);			// Quaternion.
mat p1_es(3,1);			// Vector expressing the position of the 1st footpoint in the inertial frame.
mat p2_es(3,1);			// Vector expressing the position of the 2nd footpoint in the inertial frame.
mat p3_es(3,1);			// Vector expressing the position of the 3rd footpoint in the inertial frame.
mat p4_es(3,1);			// Vector expressing the position of the 4th footpoint in the inertial frame.
mat bias_w(3,1);		// Bias of the gyroscope.

/////////////////////////////////////////////////////////////////////////////////////////////////////

mat P(24,24);			// Covariance matrix of the state.
mat Qd(24,24), Fd(24,24);	// Discretized covariance and dynamics of the error state.
mat R(12,12);			// Matrix corresponding to the noise of the measurements.
mat H(12,24);			// Matrix which maps the states to the measurements.
mat S(12,12);			// Innovation matrix.
mat Kal_Gain(24, 12);		// Kalman Gain matrix.


/////// Variables declared for the use of the filter.
mat qp(4,1), dx(18,1), g(3,1), y1(3,1), y2(3,1), y3(3,1), y4(3,1),y12(6,1), y34(6,1), y(12,1), dq(4,1);
mat  C(3,3),  G2, G1;

//// Declare the variables needed to run the algorithm, i.e. gyro and accel measurements, as well as leg kinematics.


mat omega(3,1);			// Gyroscope measurements, these should be obtained from the IMU.
mat f(3,1);			// Accelerometer measurements, these should be obtained from the IMU.

// Provide some values in order to check the filter.
omega[0] = omega[1] = omega[2] =2;

f[0] = f[1] = f[2] = 4;


z_leg1[0] = z_leg1[1] = z_leg1[2] = 3.0;

z_leg2[0] = z_leg2[1] = z_leg2[2] = 3.4;

z_leg3[0] = z_leg3[1] = z_leg3[2] = 2.0;

z_leg4[0] = z_leg4[1] = z_leg4[2] = 1.8;


if (first_call)
  {
	
  first_call = false;
//Initialize error 
  dx = zeros<mat>(18,1); 


//The white noise densities. They can be calculated simply by squaring the corresponding sigma value.

  wf = sigmaf*sigmaf;
  ww = sigmaw*sigmaw;
  wbw = sigmabw*sigmabw;
  wp1p = sigmawp1*sigmawp1;
  wp2p = sigmawp2*sigmawp2;
  wp3p = sigmawp3*sigmawp3;
  wp4p = sigmawp4*sigmawp4;
  

//Following is the process covariance. It is often set to a small value, since we know exactly the initial conditions. It has to be positive-definite during the whole estimation process.
  P = 0.0000001*eye<mat>(24,24);


//Discretized error state transition matrix and process covariance. They are calculated based on Van Loan's Method, referenced in Thesis.
  Fd = zeros<mat>(24,24);
  Qd = zeros<mat>(24,24);


//Measurement Noise covariance. Change this to obtain better (or worse) estimates. It needs tuning, usually it shall be around the same range of Qd.
  R = 0.0001*eye<mat>(12,12);


//Gravity vector in the inertial frame.
  g[0] = 0;
  g[1] = -9.81;
  g[2] = 0;


//Initialization of the estimates.
  r_es[0] = 0.0;
  r_es[1] = 0.3;
  r_es[2] = 0.0;

  u_es[0] = -0.1801;
  u_es[1] = 0;
  u_es[2] = 0.0;

  q_es[0] = 0.0;
  q_es[1] = 0.0;
  q_es[2] = 0.0;
  q_es[3] = 1.0;

  p1_es[0] = 0.27; /////////////////////////////////These values have to be changed based on the true configuration of the robot. They correspond
  p1_es[1] = 0.0;     ///////////////////////////// to the position of the i-th hip from the CoM expressed in the body frame.
  p1_es[2] = 0.195;

  p2_es[0] = -0.27;
  p2_es[1] = 0.0;
  p2_es[2] = 0.195;

  p3_es[0] = -0.27;
  p3_es[1] = 0.0;
  p3_es[2] = -0.195;

  p4_es[0] = 0.27;
  p4_es[1] = 0.0;
  p4_es[2] = -0.195;

  bias_w = eye<mat>(3,1)*0;
  }

///////////////////////////////////////////// HERE STARTS THE RECURSIVE EKF /////////////////////////////////


// The following represent the propagation of the state based on discrete time differential equations.
 G2 = GAMMA(omega-bias_w, dt, 2);
 G1 = GAMMA(omega-bias_w, dt, 1);

 C = quaternion2Matrix(q_es);
 qp = rot_vector2quaternion(dt*(omega-bias_w));
 
 r_es = r_es + dt*u_es + G2.t()*C.t()*f + 1/2*dt*dt*g;

 u_es = u_es + G1.t()*C.t()*f + dt*g;

 q_es = quaternion_mult(qp, q_es);


 p1_es = p1_es;
 
 p2_es = p2_es;
 
 p3_es = p3_es;

 p4_es = p4_es;

 bias_w = bias_w;



// If foot i has no ground contact, make the corresponding noise infinite, i.e. dont use it in the estimation process, otherwise go on.

if (!touch1)
{
wp1 = 10000000;}
else{
wp1 = wp1p;
}

if (!touch2)
{
wp2 = 10000000;}
else{
wp2 = wp2p;
}

if (!touch3)
{
wp3 = 10000000;}
else{
wp3 = wp3p;
}

if (!touch4)
{
wp4 = 10000000;}
else{
wp4 = wp4p;
}


 if ( (touch1 || touch2 || touch3 || touch4)) ///Check if there is any ground contact.
{

	//In the case of ground contact and its the first time, subsitute the previous position with the new position of the footpoint in the inertial frame.

C = quaternion2Matrix(q_es);

	if (first_touch1)
	{
		//Calculate the new position of the foot based on the first touch event.
		p1_es = r_es + C*z_leg1;
	}
	
	if (first_touch2)
	{
		//Calculate the new position of the foot based on the first touch event.
		p2_es = r_es + C*z_leg2;
	}

	if (first_touch3)
	{
		//Calculate the new position of the foot based on the first touch event.
		p3_es = r_es + C*z_leg3;
	}

	if (first_touch4)
	{
		//Calculate the new position of the foot based on the first touch event.
		p4_es = r_es + C*z_leg4;
	}


//Calculate Fd, Qd.
 Disc_Dynamics(f, omega, q_es, wf, ww, wbw, wp1, wp2, wp3, wp4, dt, Fd, Qd); 

//Propagate Covariance.
 P = Fd*P*Fd.t() + Qd; 

// Predict measurements.
 H = measur( q_es, p1_es, p2_es, p3_es, p4_es, r_es);



/// Calculate the difference between true and predicted measurements.
 y1 = z_leg1 - C*(p1_es - r_es); 
 y2 = z_leg2 - C*(p2_es - r_es);
 y3 = z_leg3 - C*(p3_es - r_es);
 y4 = z_leg4 - C*(p4_es - r_es);

// Make them a matrix.
 y12 = join_vert(y1, y2);
 y34 = join_vert(y3, y4);
 y = join_vert(y12, y34);

//Calculate innovation matrix.
 S = H*P*H.t() + R;
 
//Calculate Kalman Gain.
 Kal_Gain = P*H.t()*inv(S); 

//Calculate the correction based on the difference of the measurements, scaled by the Kalman Gain.
 dx = Kal_Gain*(y);

//Correct Covariance.
P = (eye<mat>(24,24) - Kal_Gain*H)*P;

//Correct states.
 r_es = r_es + dx.rows(0,2);
 
 u_es = u_es + dx.rows(3,5);

 dq = rot_vector2quaternion(dx.rows(6,8));
 q_es = quaternion_mult(dq, q_es);


 p1_es = p1_es + dx.rows(9, 11);
 p2_es = p2_es + dx.rows(12, 14);
 p3_es = p3_es + dx.rows(15, 17);
 p4_es = p4_es + dx.rows(18, 20);
 
 bias_w = bias_w + dx.rows(21,23);

}

//////////////////////////// HERE ENDS THE RECURSIVE EKF ///////////////////////////
  
  return 0;
}


double factorial(uint16_t n)
{
//Returns the factorial of an integer.

	double ret = 1.0;
	
	while(n>0)
	{
		ret *= (double)n;
		n--;
	}
	
	return ret;
}

mat Skew(mat vIn)
{
//Returns the skew matrix of a 3x3 vector.

	mat skM(3,3);
	
	skM << 0 << -vIn[2] << vIn[1] << endr << vIn[2] << 0 << -vIn[0] << endr << -vIn[1] << vIn[0] << 0 << endr;
	
	return skM;
}

mat GAMMA(mat omega, double dt, double k)
{
// Returns the Gamma Matrix used in the discrete time differential equations. Read thesis to check the equations.

/*
Inputs:

omega ---> Angular rates. 				3x1 vector

dt ---> Time between 2 subsequent measurements.		Scalar

k ---> Rank of the Gamma Matrix.			Scalar

*/

	mat retM(3,3, fill::zeros);
	double b = ((int32_t)k)%2;
	double m = (k-b)/2;
	double sum = 0.0, sign, ser_prod, skew_gain, skew_sqr_gain, cosw, sinw;
	double norma = norm(omega);
	uint16_t n;

	if(b == 0.0)
	{
		if(norma > 0.1)
		{
			sinw = pow(-1.0, m)*sin(norma*dt);
			for(n=0; n<m; n++)
			{
				sign = pow(-1.0, (double)(m+n+1.0));
				ser_prod = pow((norma*dt),(double)(2.0*n+1.0));
				sum += sign*ser_prod/factorial((uint16_t)(2*n+1));
			}
			
			skew_gain = 1.0/pow(norma,(2.0*m+1.0))*(sinw+sum);
			
			sum = 0.0;
			
			cosw = pow(-1.0,(m+1.0))*cos(norma*dt);
			for(n=0; n<=m; n++)
			{
				sign = pow(-1.0,(m+n));
				ser_prod = pow((norma*dt), (double)(2.0*n));
				sum += sign*ser_prod/factorial((uint16_t)(2*n));
			}

			skew_sqr_gain = 1.0/pow(norma,(2.0*m+2.0))*(cosw+sum);	
		}
		else
		{
			skew_gain = pow(dt,(2.0*m+1.0))/factorial((uint16_t)(2.0*m+1.0));
			skew_sqr_gain = pow(-1.0,(2.0*m+1.0))*(pow(dt,(2.0*m+2.0))/factorial((uint16_t)(2.0*m+2.0)));
		}
	}
	else
	{
		if(norma > 0.1)
		{
			cosw = pow(-1.0,(m+1.0))*cos(norma*dt);
			for(n=0; n<=m; n++)
			{
				sign = pow(-1.0, (double)(m+n));
				ser_prod = pow((norma*dt),(double)(2.0*n));
				sum += sign*ser_prod/factorial((uint16_t)(2*n));
			}
			
			skew_gain = 1.0/pow(norma,(2.0*m+2.0))*(cosw+sum);
			
			sum = 0.0;
			
			sinw = pow(-1.0, m+1.0)*sin(norma*dt);
			for(n=0; n<=m; n++)
			{
				sign = pow(-1.0,(m+n));
				ser_prod = pow((norma*dt), (double)(2.0*n+1.0));
				sum += sign*ser_prod/factorial((uint16_t)(2*n+1));
			}

			skew_sqr_gain = 1.0/pow(norma,(2.0*m+3.0))*(sinw+sum);
			
		}
		else
		{
			skew_gain = pow(-1.0,(2.0*m+1.0))*(pow(dt,2.0*m+2.0)/factorial((uint16_t)(2.0*m+2.0)));
			skew_sqr_gain = 0;
		}
	}
	
	retM = (pow(dt, k)/factorial((uint16_t)k))*eye<mat>(3,3) + skew_gain*Skew(omega) + skew_sqr_gain*Skew(omega)*Skew(omega);
	
	return retM;
}

mat quaternion2Matrix(mat quat)
{
//Returns the corresponding rotation matrix for a given a quaternion.

	mat q13(3,1);
	mat M(3,3);
	q13[0] = quat[0];
	q13[1] = quat[1];
	q13[2] = quat[2];
	
	
	M = (2*quat[3]*quat[3] - 1)*eye<mat>(3,3) + 2*(q13*q13.t()) - 2*quat[3]*Skew(q13);
	
	return M;
	
}


mat measur(mat qk, mat pk1, mat pk2, mat pk3, mat pk4, mat rk)
{


//Returns the matrix H, which maps the states into the measurements. For readability, we calculate each row separately and then joint them together to obtain H.

/*
Inputs:

qk  ---> Quaternion. 								4x1 vector (Scalar part is last)

pk1 ---> Position of 1st footpoint in the inertial frame.			3x1 vector

pk2 ---> Position of 2nd footpoint in the inertial frame.			3x1 vector

pk3 ---> Position of 3rd footpoint in the inertial frame.			3x1 vector

pk4 ---> Position of 4th footpoint in the inertial frame.			3x1 vector

rk ---> Position of the CoM in the inertial frame.				3x1 vector

*/



//Declate matrices needed for every row.
	mat H_meas(12, 24), H1(3, 24), H2(3, 24), H3(3, 24), H4(3, 24), H12(6, 24), H34(6, 24);
	mat hz1(3, 6), hz2(3, 6), hz3(3, 6), hz4(3, 6), h_col1(3, 12), h_col2(3, 12);

//Initialize them to avoid randomness.	
	hz1= zeros<mat>(3, 6); hz2 = zeros<mat>(3, 6); hz3 = zeros<mat>(3, 6); hz4 = zeros<mat>(3, 6);
	H1 = zeros<mat>(3,24); H2 = zeros<mat>(3,24); H3 = zeros<mat>(3,24); H4 = zeros<mat>(3,24);
	H12 = zeros<mat>(6, 24); H34 = zeros<mat>(6, 24); h_col1 = zeros<mat>(3,12); h_col2 = zeros<mat>(3,12);
	
	mat Cq = quaternion2Matrix(qk);
	
//Computethe first row.
	hz1 = join_horiz(-Cq, zeros<mat>(3,3));
	hz2 = join_horiz(Skew(Cq*(pk1-rk)), Cq);
	hz3 = join_horiz(zeros<mat>(3,3), zeros<mat>(3,3));
	hz4 = join_horiz(zeros<mat>(3,3), zeros<mat>(3,3));
	h_col1 = join_horiz(hz1, hz2);
	h_col2 = join_horiz(hz3, hz4);

	H1 = join_horiz(h_col1, h_col2);

//Compute the second row.
	hz1 = join_horiz(-Cq, zeros<mat>(3,3));
	hz2 = join_horiz(Skew(Cq*(pk2-rk)), zeros<mat>(3,3));
	hz3 = join_horiz(Cq, zeros<mat>(3,3));
	hz4 = join_horiz(zeros<mat>(3,3), zeros<mat>(3,3));
	h_col1 = join_horiz(hz1, hz2);
	h_col2 = join_horiz(hz3, hz4);

	H2 = join_horiz(h_col1, h_col2);

//Compute the third row.
	hz1 = join_horiz(-Cq, zeros<mat>(3,3));
	hz2 = join_horiz(Skew(Cq*(pk3-rk)), zeros<mat>(3,3) );
	hz3 = join_horiz(zeros<mat>(3,3), Cq);
	hz4 = join_horiz(zeros<mat>(3,3), zeros<mat>(3,3));
	h_col1 = join_horiz(hz1, hz2);
	h_col2 = join_horiz(hz3, hz4);

	H3 = join_horiz(h_col1, h_col2);

//Compute the fourth row.
	hz1 = join_horiz(-Cq, zeros<mat>(3,3));
	hz2 = join_horiz(Skew(Cq*(pk4-rk)), zeros<mat>(3,3) );
	hz3 = join_horiz(zeros<mat>(3,3), zeros<mat>(3,3));
	hz4 = join_horiz( Cq, zeros<mat>(3,3));
	h_col1 = join_horiz(hz1, hz2);
	h_col2 = join_horiz(hz3, hz4);

	H4 = join_horiz(h_col1, h_col2);

//Parse rows to obtain columns and finally H.
	H12 = join_vert(H1, H2);
	H34 = join_vert(H3, H4);
	H_meas = join_vert(H12, H34);
	return H_meas;
}

mat rot_vector2quaternion(mat vIn)
{
//Maps a rotation vector to the corresponding quaternion. Care that if the norm of the rotation vector is close to zero, calculate limits and change formula.

	mat M(1,4);
	double q1, q2, q3, q4;
	double norma = sqrt(vIn[0]*vIn[0] + vIn[1]*vIn[1] + vIn[2]*vIn[2]);
	
	if(norma > 0.1)
	{
		q1 = vIn[0]*sin(norma/2.0)/norma;
		q2 = vIn[1]*sin(norma/2.0)/norma;
		q3 = vIn[2]*sin(norma/2.0)/norma;
		q4 = cos(norma/2.0);
	}
	else
	{
		q1 = vIn[0]/2.0;
		q2 = vIn[1]/2.0;
		q3 = vIn[2]/2.0;
		q4 = 1.0;
	}
	
	M << q1 << q2 << q3 << q4 << endr;
	
	return M.t();
}

void Disc_Dynamics(mat f, mat omega, mat q, double wf, double ww, double wbw, double wp1, double wp2, double wp3, double wp4,  double dt, mat& Fd, mat&  Qd)
{


//Returns the discretized error transition matrix and covariance matrix, namely Fd and Qd. We first form the continuous time matrices and then compute the discretized ones based on Van Loan's method.
/*
Inputs:

f --->accelerations. 		 3x1 vector.

omega---> angular rates		 3x1 vector.

q ---> quaternion.  		 4x1 vector (Scalar part is last)

wf ---> White noise density of the accelerometer. 	Scalar

ww---> White noise density of the gyroscope.		Scalar

wbw---> White noise density of the gyroscope's bias.	Scalar

wp1---> White noise density of the first footpoint. 	Scalar

wp2---> White noise density of the first footpoint. 	Scalar

wp3---> White noise density of the first footpoint. 	Scalar

wp4---> White noise density of the first footpoint. 	Scalar

dt---> Time between two subesequent measurements.

*/


mat M(48, 48), G(24, 21), W(21,21), Qs(24,24);

mat C=quaternion2Matrix(q);

Fd(span(0,2), span(3,5)) = eye<mat>(3,3);

Fd(span(3,5), span(6,8)) = -C.t()*Skew(f);
Fd(span(3,5), span(12,14)) = -C.t();

Fd(span(6,8), span(6,8)) = -Skew(omega);
Fd(span(6,8), span(15,17)) = -eye<mat>(3,3);


G = zeros<mat>(24,21);

G(span(3,5),span(0,2)) = -C.t();
G(span(6,8),span(3,5)) = -eye<mat>(3,3);
G(span(9,11),span(6,8)) = C.t();
G(span(12,14), span(9,11)) = C.t();
G(span(15,17), span(12,14)) = C.t();
G(span(18,20), span(15,17)) = C.t();
G(span(21,23), span(18,20)) = eye<mat>(3,3);

W = zeros<mat>(21,21);

W(span(0,2),span(0,2))=wf*eye<mat>(3,3);
W(span(3,5),span(3,5))=ww*eye<mat>(3,3);
W(span(6,8),span(6,8))=wp1*eye<mat>(3,3);
W(span(9,11),span(9,11))=wp2*eye<mat>(3,3);
W(span(12,14),span(12,14))=wp3*eye<mat>(3,3);
W(span(15,17),span(15,17))=wp4*eye<mat>(3,3);
W(span(18,20),span(18,20))=wbw*eye<mat>(3,3);


Qs = G*W*G.t();


M(span(0,23), span(0,23)) = Fd*dt;

M(span(0, 23), span(24, 47)) = Qs*dt;

M(span(24,47), span(0,23)) = zeros<mat>(24,24);

M(span(24,47), span(24,47)) = Fd.t()*dt;


mat B = expmat(M);

mat T = B(span(24,47), span(24,47));
Fd = T.t();

T = B(span(0,23), span(24, 47));
Qd = Fd*T;


}

mat quaternion_mult(mat qin, mat pin)
{
//quaternion multiplication, scalar part of the quaternion is in the end
mat pr(4,1);

pr[0] = qin[3]*pin[0] + qin[2]*pin[1] - qin[1]*pin[2] + qin[0]*pin[3];

pr[1] = -qin[2]*pin[0] + qin[3]*pin[1] + qin[0]*pin[2] + qin[1]*pin[3];

pr[2] = qin[1]*pin[0] - qin[0]*pin[1] + qin[3]*pin[2] + qin[2]*pin[3];

pr[3] = -qin[0]*pin[0] - qin[1]*pin[1] - qin[2]*pin[2] + qin[3]*pin[3];

return pr;

}

