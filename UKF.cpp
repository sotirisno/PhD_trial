#include <iostream>
#include <inttypes.h>
#include <armadillo>
#include <cmath>




using namespace std;
using namespace arma;

#define pi 3.14159265359



mat rot_vector2quaternion(mat vIn);

mat quaternion_mult(mat qin, mat pin);

void Quaternion_sigma_points(mat x_m, mat P, mat Q, double a, double b, double k, double q_pos, mat &x_q, mat &Wqm, mat &Wqc);

mat propagate(mat xm, mat omega, double dt, double q_pos);

mat UT_mean(mat xq, mat Wq, double q_pos);

mat measur_map(mat xq, double cols, double q_pos);

mat measur_mean(mat yq, mat Wqm, double num_cols);

mat state_covariance( mat xq, mat xm, mat Wqc, double q_pos);

void cov_measur(mat yq, mat ym, mat Wqc, mat R, mat &P);

mat cov_state_measur(mat xq, mat xm, mat Wqc, mat yq, mat ym, double q_pos);

mat angle2quaternion( double yaw, double pitch, double roll);

mat q_encoder_measurement(mat spring_pos, mat servo_pos);

void Unscented_Kalman(mat x_m, mat Pxx, mat Q, double a, double b, double k, double q_pos, bool full_stance, mat spring_pos, mat servo_pos, mat R, mat omega, double dt);



// Main Function
int main(int argc, char **argv)
{

//////////////////// Variables used ////////////////////////////


mat omega(3,1);  	// Gyroscope measurements.
mat spring_pos(4,1); 	// Spring position. The i-th element is the reading of the i-th spring.
mat servo_pos(4, 1);	// Servo position measurements. The i-th elements corresponds to the reading of the i-th encoder.
mat q_enc(4, 1);	// Quaternion calculated from the leg kinematics.
mat x_m(7, 1);		// Mean of the state. The first four elements is the quaternion, whereas the next three are the biases. This is returned 				// after each iteration.
mat Pxx(6, 6);		// Covariance matrix of the state. It can be changed to different values, to obtain better estimates.
mat Q(6, 6);		// Covariance of the process noise. Change this according to the thesis.
mat R(3, 3);		// Covariance of the measurement noise. Change this for tuning purposes.
mat ym(4, 1);		// Mean of the predicted measurements.
mat yq(7, 13);		// Matrix of the sigma points used in the prediction of the measurements.
mat Pyy(3, 3);		// Matrix of the covariance of the measurements.
mat Pxy(6, 3);		// Cross-covariance matrix between the state and the measurements.
mat Kal_Gain(6, 3);	// Kalman Gain matrix.
double a, b, k;		// Scalars used for tuning of the UKF.
double q_pos;		// Starting position of the quaternion state. Default is 0 (1 in Matlab).
double dt;		// Time between two subsequent measurements.
mat x_q(7, 13);		// State sigma points matrix.
mat Wqm(13,1);		// Matrix containing the weights used in the mean calculation.
mat Wqc(13,1);		// Matrix containing the weights used in the covariance calculation. 
mat xn_q( 7, 13);	// Matrix containing the predicted sigma points.
bool full_stance = true;	// Boolean declaring the event of a full stance phase.
bool first_call = true; 	// If is the first time of the call to UKF, then initiliazation of the state is done inside the filter.

					
mat mul(3, 1), mul_t(1, 3);						///////////////////////////////////////////////
mat inv_ym(4, 1), qz(4, 1), dq(4, 1), dqx(3, 1), dx(6, 1), dxq(4, 1);	// Variables declared for used in the filter.//
double N = sqrt(Pxx.n_elem);						///////////////////////////////////////////////





 if (first_call){

 first_call = false;

//double po;

x_m[0] = 0;
x_m[1] = 0;
x_m[2] = 0;
x_m[3] = 1;
x_m[4] = 0;
x_m[5] = 0;
x_m[6] =0;

a = 0.03; b = 2; k = -2; q_pos = 0; dt = 0.001;


Pxx = 0.00001*eye<mat>(6,6);

Q = 0.000001*4*eye<mat>(6, 6);

x_q = zeros<mat>(7, 13);

Wqm = zeros<mat>(13, 1); Wqc = zeros<mat>(13, 1);

R  = 0.00001*eye<mat>(3,3); Pyy = zeros<mat>(3, 3);




}


/*
 //  Tested.
Quaternion_sigma_points(x_m, P, Q, a, b, k, q_pos, x_q, Wqm, Wqc);

x_m = UT_mean(x_q, Wqm, q_pos);

Pxx = state_covariance( x_q, x_m, Wqc, q_pos);

yq =  measur_map( x_q, Wqc.n_elem, q_pos);

ym = measur_mean( yq, Wqm, Wqm.n_elem);

cov_measur( yq, ym, Wqc, R, Pyy);

Pxy = cov_state_measur(x_q, x_m, Wqc, yq, ym, q_pos);

//cout<<Pxy<<endl;

q_enc =  encoder_measurement(spring_pos, servo_pos);

cout<<q_enc<<endl;
*/


//void Unscented_Kalman(mat x_m, mat Pxx, mat Q, double a, double b, double k, double q_pos, bool full_stance, mat spring_pos, mat servo_pos, mat R, mat omega, double dt){

/* If you delcare this as a function the following variables should be declared inside the function, whereas the above variables should be declared as globals.


mat q_enc(4, 1);	// Quaternion calculated from the leg kinematics.
mat ym(4, 1);		// Mean of the predicted measurements.
mat yq(7, 13);		// Matrix of the sigma points used in the prediction of the measurements.
mat Pyy(3, 3);		// Matrix of the covariance of the measurements.
mat Pxy(6, 3);		// Cross-covariance matrix between the state and the measurements.
mat Kal_Gain(6, 3);	// Kalman Gain matrix.
mat x_q(7, 13);		// State sigma points matrix.
mat Wqm(13,1);		// Matrix containing the weights used in the mean calculation.
mat Wqc(13,1);		// Matrix containing the weights used in the covariance calculation. 
mat xn_q( 7, 13);	// Matrix containing the predicted sigma points.

					
mat mul(3, 1), mul_t(1, 3);						///////////////////////////////////////////////
mat inv_ym(4, 1), dq(4, 1), dqx(3, 1), dx(6, 1), dxq(4, 1);	// Variables declared for used in the filter.//
double N = sqrt(Pxx.n_elem);						///////////////////////////////////////////////
*/




// UKF starts.

// Calculate sigma points and the corresponding weights.
Quaternion_sigma_points(x_m, Pxx, Q, a, b, k, q_pos, x_q, Wqm, Wqc);

// Propagate each sigma point to obtain the prior belief.
	for (int i = 0; i < 2*N + 1; i++)
	{
	xn_q.col(i) = propagate( x_q.col(i), omega, dt, q_pos);
	}


//Calculate current mean of propagated sigma points.
x_m = UT_mean(xn_q, Wqm, q_pos);

//If full-stance is detected, go on and correct measurements.

	if (full_stance)
	{

	// Predict measurements and their mean.
	yq = measur_map(xn_q, 2*N + 1, q_pos);
	ym = measur_mean(yq, Wqm, 2*N + 1);

	// Calculate state covariance, measurement covariance and cross covariance matrices.
	Pxx = state_covariance( xn_q, x_m, Wqc, q_pos);
	cov_measur( yq, ym, Wqc, R, Pyy);
	Pxy = cov_state_measur(x_q, x_m, Wqc, yq, ym, q_pos);
	
	// Compute Kalman gain.
	Kal_Gain = Pxy*inv(Pyy);

	// Calculate the inverse quaternion of the measurements, used for the innovation step.
	inv_ym(span(0, 2), 0) = -ym(span(0, 2), 0);
	inv_ym(3) = ym(3,0);

	// Calculate inclination of the robot based on encoder measurements.
	q_enc = q_encoder_measurement(spring_pos, servo_pos);
	
	// Calculate innovation, i.e. difference between true and predicted measurements.
	dq = quaternion_mult(q_enc, inv_ym);
	dqx = dq(span(0, 2), 0);

	// Calculate correction.
	dx = Kal_Gain*dqx;

	// Correct the covariance and the state.
	Pxx = Pxx - Kal_Gain*Pyy*Kal_Gain.t();

	dxq(span(0, 2), 0) = dx(span(q_pos, q_pos + 2), 0);
	
	dxq(3, 0) = sqrt(1 - dxq(0)*dxq(0) - dxq(1)*dxq(1) - dxq(2)*dxq(2));
	
	x_m(span(q_pos, q_pos + 3),0 ) = quaternion_mult(dxq, x_m(span(q_pos, q_pos + 3), 0));
	
	}

//}
return 0;
}



mat rot_vector2quaternion(mat vIn)
{
//Maps a rotation vector to the corresponding quaternion. Care that if the norm of the rotation vector is close to zero,
//calculate limits and change formula.

	mat M(1,4);
	double q1, q2, q3, q4;
	double norma = sqrt(vIn[0]*vIn[0] + vIn[1]*vIn[1] + vIn[2]*vIn[2]);
	
	if(norma > 0.1)
	{
		q1 = vIn[0]*sin(norma/2.0)/norma;
		q2 = vIn[1]*sin(norma/2.0)/norma;
		q3 = vIn[2]*sin(norma/2.0)/norma;
		q4 = cos(norma/2.0);
	}
	else
	{
		q1 = vIn[0]/2.0;
		q2 = vIn[1]/2.0;
		q3 = vIn[2]/2.0;
		q4 = 1.0;
	}
	
	M << q1 << q2 << q3 << q4 << endr;
	
	return M.t();
}


mat quaternion_mult(mat qin, mat pin)
{
//Quaternion multiplication, scalar part of the quaternion is in the end
mat pr(4,1);

pr[0] = qin[3]*pin[0] + qin[2]*pin[1] - qin[1]*pin[2] + qin[0]*pin[3];

pr[1] = -qin[2]*pin[0] + qin[3]*pin[1] + qin[0]*pin[2] + qin[1]*pin[3];

pr[2] = qin[1]*pin[0] - qin[0]*pin[1] + qin[3]*pin[2] + qin[2]*pin[3];

pr[3] = -qin[0]*pin[0] - qin[1]*pin[1] - qin[2]*pin[2] + qin[3]*pin[3];

return pr;

}


void Quaternion_sigma_points(mat x_m, mat P, mat Q, double a, double b, double k, double q_pos, mat &x_q, mat &Wqm, mat &Wqc)
{
/* 
// This function calculates the sigma points based on the current mean x_m, which is a vector of 7x1. The other inputs cosist of:
// P --> Covariance matrix, 13x13.
// Q--> Process noise matrix, 13x13.
// a, b, k--> Scalars needed for the Unscented Transformation.
// q_pos --> The starting position of the quaternion in the state vector. Default is 0 in C and 1 in Matlab.
//
// Outpurs the sigma points matrix as well as the corresponding mean and covariance weights.
*/

double N = sqrt(P.n_elem);
double lamda, gam;
mat q_es(4, 1), U(6, 6), x_w(3, 1), U_i(6, 1), dq(4, 1), q_sigma(4, 1);


lamda = a*a*(N + k) - N;

gam = N + lamda;


Wqm = zeros<mat>(2*N + 1, 1);


Wqc = zeros<mat>(2*N + 1, 1);

Wqm(0) = lamda/gam;

Wqc(0) = lamda/gam + (1 - a*a + b);

Wqm( span(1, 2*N), 0) = 1/(2*(N + lamda))*ones(2*N, 1);

Wqc(span(1, 2*N), 0) = 1/(2*(N + lamda))*ones(2*N, 1);


q_es = x_m(span(q_pos, q_pos + 3), 0);


U = chol(gam*(P + Q));



x_q.col(0) = x_m(span(0, 6), 0);


for (int i=0; i < N; i++)
{

U_i(span(0, 5), 0) = (U.row(i)).t();

x_w[0] = U_i(q_pos);  x_w[1] = U_i(q_pos + 1); x_w[2] = U_i(q_pos + 2); 

dq = rot_vector2quaternion(x_w);

q_sigma = quaternion_mult(dq, q_es);

x_q(q_pos, i+1) = q_sigma(0);
x_q(q_pos + 1, i+1) = q_sigma(1);
x_q(q_pos + 2, i+1) = q_sigma(2);
x_q(q_pos + 3, i+1) = q_sigma(3);
x_q(q_pos + 4, i+1) = x_m(q_pos + 4, 0) + U(i, 3);
x_q(q_pos + 5, i+1) = x_m(q_pos + 5, 0) + U(i, 4);
x_q(q_pos + 6, i+1) = x_m(q_pos + 6, 0) + U(i, 5);

}


for (int i=0; i < N ; i++)
{

U_i(span(0, 5), 0) = (U.row(i)).t();

x_w[0] = U_i(q_pos);  x_w[1] = U_i(q_pos + 1); x_w[2] = U_i(q_pos + 2); 

dq = rot_vector2quaternion(x_w);
dq[0] = -dq[0]; dq[1] = -dq[1]; dq[2] = -dq[2];

q_sigma = quaternion_mult(dq, q_es);


x_q(q_pos, N + i + 1) = q_sigma(0);
x_q(q_pos + 1, N + i + 1) = q_sigma(1);
x_q(q_pos + 2, N + i + 1) = q_sigma(2);
x_q(q_pos + 3, N + i + 1) = q_sigma(3);
x_q(q_pos + 4, N + i + 1) = x_m(q_pos + 4, 0) - U(i, 3);
x_q(q_pos + 5, N + i + 1) = x_m(q_pos + 5, 0) - U(i, 4);
x_q(q_pos + 6, N + i + 1) = x_m(q_pos + 6, 0) - U(i, 5);


}
}


mat propagate(mat xm, mat omega, double dt, double q_pos)
{
/* This function propagates every sigma point based on the discretized differentia equations.
// Inputs:
// x_m --> Sigma point, vector 7x1.
// omega --> Angular rates in rad/sec. Vector 3x1.
// dt --> Time between two subsequent measurements. Scalar.
// q_pos --> Starting position of the quaternion in the state. Default is 0.
//
// Outputs the propagated sigma point. A vector 7x1.
*/

mat xn(7, 1), qp(4,1);

qp = rot_vector2quaternion(dt*(omega - xm(span(q_pos + 4, q_pos +6), 0)));

xn(span(q_pos, q_pos +3), 0) = quaternion_mult(qp, xm(span(q_pos, q_pos + 3), 0));
xn(span(q_pos + 4, q_pos + 6), 0) = xm(span(q_pos + 4, q_pos +6), 0);

return xn;

}


mat UT_mean(mat xq, mat Wq, double q_pos)
{
/* Outputs the current mean of the set of sigma points xq.
// Inputs:
// Wq --> Matrix of the mean weights, calculated by the function quaternion_sigma_points.
// q_pos --> see description of quaternion_sigma_points.
*/


double p, norm_q_sum;
mat m(7, 1), q_sum(4, 1);

m = zeros<mat>(7, 1);
q_sum = zeros<mat>(4,1);

p = Wq.n_elem;

q_sum = Wq(0)*xq(span(q_pos, q_pos + 3), 0);


m(span(q_pos + 4, q_pos + 6), 0) = Wq(0)*xq(span(q_pos + 4, q_pos + 6), 0);

	for (int i=1; i <= p-1; i++)
	{
	
	q_sum = q_sum + Wq(i)*xq(span(q_pos, q_pos + 3), i);
	
	m(span(q_pos + 4, q_pos + 6), 0) = m(span(q_pos + 4, q_pos + 6), 0) + Wq(i)*xq(span(q_pos + 4, q_pos + 6), i);

	}


norm_q_sum = norm(q_sum, 2);
q_sum = q_sum/norm_q_sum;
m(span(q_pos, q_pos + 3), 0) = q_sum;

return m;
}


mat measur_map(mat xq, double cols, double q_pos)
{
/* Outputs the predicted set of measurements, given a set of sigma points xq. The input cols, declares the columns of the 
// matrix xq.
*/

mat yq(4, cols);

yq = zeros<mat>(4, cols);

	for (int i = 0; i < cols; i++)
	{
	yq(span(q_pos, q_pos + 3), i) = xq(span(q_pos, q_pos + 3), i);
	}
	
	return yq;
}

mat measur_mean(mat yq, mat Wqm, double num_cols)
{
/* Outputs the mean of the measurements, given a measurement sigma point set yq.
*/

mat ym(4, 1), q_sum(4, 1);

q_sum = zeros<mat>(4, 1);

q_sum(span(0,3), 0) = Wqm(0)*yq(span(0, 3), 0);

	for (int i=1; i<num_cols; i++)
	{
	q_sum(span(0,3), 0) = q_sum(span(0,3), 0) + Wqm(i)*yq(span(0, 3), i);
	}

ym(span(0, 3), 0) = q_sum/norm(q_sum, 2);

return ym;

}

mat state_covariance( mat xq, mat xm, mat Wqc, double q_pos)
{
/* Outputs the covariance matrix of the state, namely P in equations. Inputs are the set of sigma points xq, the current mean of the set
// the covariance weights, calculated from the function quaternion_sigma_points. The same idea is used for computing the measurement covar-
// iance matrix as well as the cross-covariance matrix, between the state and the measurements.
*/


double N, num_cols;
N = xm.n_elem; num_cols = Wqc.n_elem;

mat Kq(N-1, N-1), inv_qm(4, 1), dq(4, 1), dqc(3, 1), db(3, 1), Pxx(N - 1, N - 1);

Kq = zeros<mat>(N-1, N-1);

inv_qm(span(0, 2), 0) = -xm(span(q_pos, q_pos +2), 0);
inv_qm(3) = xm(3,0); 

	for (int i=0; i<num_cols; i++)
	{
	dq = quaternion_mult(xq(span(q_pos, q_pos + 3), i), inv_qm);
	dqc = dq(span(q_pos, q_pos + 2), 0);
	Kq(span(q_pos, q_pos + 2), span(q_pos, q_pos + 2)) = Kq(span(q_pos, q_pos + 2), span(q_pos, q_pos + 2)) + Wqc(i) *dqc*(dqc.t());
	
	db = xq(span(q_pos + 4, q_pos + 6), i) - xm(span(q_pos + 4, q_pos + 6), 0);
	Kq(span(q_pos + 3, (N - 1) - 1), span(q_pos + 3, (N - 1) -1)) = Kq(span(q_pos + 3, (N - 1) - 1), span(q_pos + 3, (N - 1) -1)) + Wqc(i)*db*(db.t());
	}

Pxx = Kq;
return Pxx;

}


void cov_measur(mat yq, mat ym, mat Wqc, mat R, mat &P)
{

double num_cols;
mat Kq(3, 3), inv_qm(4, 1), dq(4, 1), dqc(3, 1);

num_cols = Wqc.n_elem;

inv_qm(span(0, 2), 0) = -ym(span(0, 2), 0);
inv_qm(3) = ym(3,0); 


	for (int i = 0; i < num_cols; i++)
	{

	dq = quaternion_mult(yq(span(0, 3), i), inv_qm);
	dqc = dq(span(0, 2), 0);
	Kq = Kq + Wqc(i)*dqc*(dqc.t());
	
	}

P = Kq + R;

}

mat cov_state_measur(mat xq, mat xm, mat Wqc, mat yq, mat ym, double q_pos)
{

double N = xm.n_elem;
double num_col = Wqc.n_elem;
mat Kqy(N-1, 3);
mat  inv_ym(4, 1), inv_qxm(4, 1), dqy(4, 1), dqyc(3, 1), dqx(4, 1), dqxc(3, 1), db(3,1);


inv_ym(span(0, 2), 0) = -ym(span(0, 2), 0);
inv_ym(3) = ym(3,0);

inv_qxm(span(0, 2), 0) = -xm(span(q_pos + 0, q_pos + 2), 0);
inv_qxm(3) = xm(3,0);

	for (int i = 0; i < num_col; i++)
	{
	dqy = quaternion_mult(yq(span(0, 3), i), inv_ym);

	dqyc = dqy(span(0, 2), 0);

	dqx = quaternion_mult(xq(span(q_pos + 0, q_pos + 3), i), inv_qxm);
	dqxc = dqx(span(0, 2), 0);

	db = xq(span(q_pos + 4, q_pos + 6), i) - xm(span(q_pos + 4, q_pos + 6), 0);

	Kqy(span(0, 2), span(0, 2)) = Kqy(span(0, 2), span(0, 2)) + Wqc(i)*dqxc*(dqyc.t());
	Kqy(span(3, 5), span(0, 2)) = Kqy(span(3, 5), span(0, 2)) + Wqc(i)*db*(dqyc.t());
	}

return Kqy;

}

mat angle2quaternion( double yaw, double pitch, double roll)
{
/* Returns the quaternion from a given set of angles a1, a2, a3.
** The angles shall be in radians and the quaternion calculated 
** is based on the 'zyx' rotation.
*/

mat cangles(3, 1), sangles(3, 1), q(4, 1);

cangles(0) = cos(yaw/2); cangles(1) = cos(pitch/2); cangles(2) = cos(roll/2);
sangles(0) = sin(yaw/2); sangles(1) = sin(pitch/2); sangles(2) = sin(roll/2);


q(0) = cangles(0)*cangles(1)*cangles(2) + sangles(0)*sangles(1)*sangles(2);
q(1) = cangles(0)*cangles(1)*sangles(2) - sangles(0)*sangles(1)*cangles(2);
q(2) = cangles(0)*sangles(1)*cangles(2) + sangles(0)*cangles(1)*sangles(2);
q(3) = sangles(0)*cangles(1)*cangles(2) - cangles(0)*sangles(1)*sangles(2);

return q;
}

mat q_encoder_measurement(mat spring_pos, mat servo_pos)
{
/* This function calculates the quaternion, based on the encoder measurements 
** and on the assumption that we have a full stance phase. The inputs shall be
** in meters for the springs and in radians for the servos. The configuation 
** supposed is that of the Scout platfor, as implemented in the Webots environment.
*/

// Initial lengths of the platform.

double l1, l2, l3, l4, phi, theta, num, denum;

// Vectors denoting the position of each hip in bodyframe.

mat bb1(3, 1), bf2(3, 1), bf3(3, 1), bb4(3, 1), R(3, 3), q(4, 1);

// Calculate the vectors from each hip to each footpoint.

mat hip2foot1(3, 1), hip2foot2(3, 1), hip2foot3(3, 1), hip2foot4(3, 1), unit_vector(3, 1);
mat footpoint1(3, 1), footpoint2(3, 1), footpoint3(3, 1), footpoint4(3, 1), e2(3, 1), e1(3, 1);

// Initialize.
// To do: Write what the bb means.
// The following vectors correspond to the position of the i-th hip with respect to the center of the mass of the robot.

bb1(0) = 0.27; bb1(1) = 0.0; bb1(2) = 0.195;

bf2(0) = -0.27; bf2(1) = 0.0; bf2(2) = 0.195;

bf3(0) = -0.27; bf3(1) = 0.0; bf3(2) = -0.195;

bb4(0) = 0.27; bb4(1) = 0.0; bb4(2) = -0.195;

unit_vector(1) = -1;


//Calculate the current spring lengths.

l1 = 0.3 - spring_pos(0); l2 = 0.3 - spring_pos(1); l3 = 0.3 - spring_pos(2); l4 = 0.3 - spring_pos(3);

//Calculate the vectors denoting the position of each footpoint from each hip, and then calculate footpoints position expressed in body frame.


// Calculate the rotation matrix corresponding to the rotation of servo 1.

R(0, 0) = cos(servo_pos(0)); R(0, 1) = -sin(servo_pos(0)); R(1, 0) = sin(servo_pos(0)); R(1, 1) = cos(servo_pos(0)); R(2, 2) = 1;

hip2foot1 = l1*R*unit_vector; // vector expressing the position of the first footpoint with respect to its hip.

footpoint1 = hip2foot1 + bb1; // vector expressing the position of the first footpoint with respect to the COM.

//Footpoint 2. The meaning of the vectors are the same as stated above.

R(0, 0) = cos(servo_pos(1)); R(0, 1) = -sin(servo_pos(1)); R(1, 0) = sin(servo_pos(1)); R(1, 1) = cos(servo_pos(1)); R(2, 2) = 1;

hip2foot2 = l2*R*unit_vector;

footpoint2 = hip2foot2 + bf2;

//Footpoint 3.

R(0, 0) = cos(servo_pos(2)); R(0, 1) = -sin(servo_pos(2)); R(1, 0) = sin(servo_pos(2)); R(1, 1) = cos(servo_pos(2)); R(2, 2) = 1;

hip2foot3 = l3*R*unit_vector;

footpoint3 = hip2foot3 + bf3;

//Footpoint 4.

R(0, 0) = cos(servo_pos(3)); R(0, 1) = -sin(servo_pos(3)); R(1, 0) = sin(servo_pos(3)); R(1, 1) = cos(servo_pos(3)); R(2, 2) = 1;

hip2foot4 = l4*R*unit_vector;

footpoint4 = hip2foot4 + bb4;

e2 = footpoint3 - footpoint2;	// Vector describing the distance between footpoint 3 and footpoint 2. Expressed in body frame.

e1 = footpoint4 - footpoint3;	// Vector describing the distance between footpoint 4 and footpoint 3. Expressed in body frame.

R = zeros<mat>(3, 3);

num = e2(2)*e1(0) - e1(2)*e2(0);

denum = e1(1)*e2(0) - e2(1)*e1(0);

phi = atan(num/denum);		// Roll

theta = (sin(phi)*e1(1) + cos(phi)*e1(2))/e1(0);	// Pitch


q = angle2quaternion(theta, 0, phi);

return q;

}




